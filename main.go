package main

import (
	"crypto/sha256"
	"crypto/sha512"
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	if len(os.Args) > 2 {
		arg := os.Args[1]

		var valid bool
		for _, sha := range []string{"512", "384", "256"} {
			if arg == sha {
				valid = true
				break
			}
		}
		if !valid {
			fmt.Printf("Unknown sum method %s\n", arg)
			os.Exit(1)
		}

		done := make(chan struct{})
		for i := 2; i < len(os.Args); i++ {
			go func(i int) {
				filename := string(os.Args[i])
				data, err := ioutil.ReadFile(os.Args[i])
				if err != nil {
					fmt.Printf("Failed to read file: %s\n", err.Error())
					done <- struct{}{}
					return
				}
				var c1 string
				if len(os.Args) > 2 {
					switch arg {
					case "384":
						c1 = fmt.Sprintf("%x", (sha512.Sum384(data)))
					case "512":
						c1 = fmt.Sprintf("%x", (sha512.Sum512(data)))
					case "256":
						c1 = fmt.Sprintf("%x", (sha256.Sum256(data)))
					default:
						panic("You are not supposed to see this.")
					}
				}
				fmt.Printf("\"%s\":%x\n", filename, c1)
				done <- struct{}{}
			}(i)
		}
		var x int
		for range done {
			x++
			if x == len(os.Args)-2 {
				break
			}
		}
	} else {
		fmt.Printf("Usage: %s 512|384|256 files...\n", os.Args[0])
		os.Exit(1)
	}
}
